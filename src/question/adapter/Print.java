package question.adapter;

public interface Print {
	abstract public void printWeak();
	abstract public void printStrong();
}
