package question.iterator;

public interface Aggregate {
	abstract public Iterator iterator();
}
