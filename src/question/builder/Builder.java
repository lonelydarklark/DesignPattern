package question.builder;

abstract public class Builder {
	abstract public void makeTitle(String title);
	abstract public void makeString(String str);
	abstract public void makeItems(String[] items);
	abstract public void close();
}
