package question.singleton;

public class Main {
	public static void main(String[] args) {
		TicketMaker t1 = TicketMaker.getInstance();
		TicketMaker t2 = TicketMaker.getInstance();
		TicketMaker t3 = TicketMaker.getInstance();

		System.out.println(t1.getTicketNumber());
		System.out.println(t2.getTicketNumber());
		System.out.println(t3.getTicketNumber());


	}
}
