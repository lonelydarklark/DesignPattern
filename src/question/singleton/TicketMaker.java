package question.singleton;

public class TicketMaker {
	private int ticketNumber = 1000;
	private static TicketMaker ticketMaker = new TicketMaker();

	private TicketMaker() {
	}

	public static TicketMaker getInstance() {
		return ticketMaker;
	}

	public synchronized int getTicketNumber(){
		return ticketNumber++;
	}
}
