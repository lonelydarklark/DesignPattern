package question.singleton;

public class Triple {
	private static Triple[] instances = new Triple[]{
		new Triple(),
		new Triple(),
		new Triple(),
	};

	private Triple() {
	}

	public static Triple getInstance(int id) {
		return instances[id];
	}
}
