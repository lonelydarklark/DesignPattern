package question.prototype.framework;

import question.prototype.framework.Product;

abstract public class ConcreateProduct implements Product{

	@Override
	public Product createClone() {
		Product p = null;
		try {
			p = (Product)clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return p;
	}
}
