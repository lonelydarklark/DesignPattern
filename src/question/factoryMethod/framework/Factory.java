package question.factoryMethod.framework;

abstract public class Factory {
	public final Product create(String owner) {
		Product p = createProduct(owner);
		registerProduct(p);
		return p;
	}

	abstract protected Product createProduct(String owner);
	abstract protected void registerProduct(Product product);
}
