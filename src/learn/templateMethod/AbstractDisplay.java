package learn.templateMethod;

abstract public class AbstractDisplay {
	abstract public void open();
	abstract public void print();
	abstract public void close();
	public final void display() {
		open();
		for (int i = 0; i < 5; i++) {
			print();
		}
		close();
	}
}
