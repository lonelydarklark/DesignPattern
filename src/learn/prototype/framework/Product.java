package learn.prototype.framework;

public interface Product extends Cloneable {
	abstract public void use(String s);
	abstract public Product createClone();
}
