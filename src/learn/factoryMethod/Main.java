package learn.factoryMethod;

import learn.factoryMethod.framework.Factory;
import learn.factoryMethod.framework.Product;
import learn.factoryMethod.idcard.IDCardFactory;

public class Main {
	public static void main(String[] args) {
		Factory factory = new IDCardFactory();
		Product card1 = factory.create("松本隆史");
		Product card2 = factory.create("まつも");
		Product card3 = factory.create("おほほ");
		card1.use();
		card2.use();
		card3.use();

	}
}
