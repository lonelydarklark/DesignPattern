package learn.factoryMethod.framework;

abstract public class Product {
	abstract public void use();
}
