package learn.astractFactory.listfactory;

import learn.astractFactory.factory.Factory;
import learn.astractFactory.factory.Link;
import learn.astractFactory.factory.Page;
import learn.astractFactory.factory.Tray;

public class ListFactory extends Factory{

	@Override
	public Link createLink(String caption, String url) {
		return new ListLink(caption, url);
	}

	@Override
	public Tray createTray(String caption) {
		return new ListTray(caption);
	}

	@Override
	public Page createPage(String title, String author) {
		return new ListPage(title, author);
	}

}
