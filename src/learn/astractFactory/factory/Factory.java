package learn.astractFactory.factory;

abstract public class Factory {
	public static Factory getFactory(String classname) {
		Factory factory = null;

		try {
			factory = (Factory) Class.forName(classname).newInstance();
		} catch (ClassNotFoundException e) {
			System.err.println("クラス" + classname + " は見つかりません。");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return factory;
	}

	abstract public Link createLink(String caption, String url);
	abstract public Tray createTray(String caption);
	abstract public Page createPage(String title, String author);
}
