package learn.astractFactory.factory;

abstract public class Link extends Item{
	protected String url;

	public Link(String caption, String url) {
		super(caption);
		this.url = url;
	}
}
