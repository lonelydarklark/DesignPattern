package learn.astractFactory.factory;

abstract public class Item {
	protected String caption;

	public Item(String caption) {
		this.caption = caption;
	}

	abstract public String makeHTML();
}
