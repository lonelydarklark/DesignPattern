package learn.iterator;

public interface Aggregate {
	abstract public Iterator iterator();
}
