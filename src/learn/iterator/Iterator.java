package learn.iterator;

public interface Iterator {
	abstract public boolean hasNext();
	abstract public Object next();
}
